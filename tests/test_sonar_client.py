import unittest.mock

import pytest

from sonar_client.client import SonarClient
from sonar_client.exceptions import SonarProjectAlreadyExistsException
from sonar_client.models import NewCodeDefinition, Project

SONAR_TOKEN = None


@unittest.mock.patch("sonar_client.session.SonarSession.get")
def test_sonar_project_search(mock_session_get):
    mock_session_get.return_value.json.return_value = {
        "paging": {"pageIndex": 1, "pageSize": 100, "total": 1},
        "components": [
            {
                "organization": "hoshiyosan",
                "key": "hoshiyosan-my-awesome-project",
                "name": "My awesome project",
                "qualifier": "TRK",
                "visibility": "public",
                "lastAnalysisDate": "2022-06-16T14:41:16+0200",
                "revision": "e806983445d408a0cade4e717bc6d14d42879080",
            },
        ],
    }

    sonar = SonarClient(SONAR_TOKEN)
    projects = sonar.search_project("hoshiyosan-my-awesome-project")
    assert len(projects) == 1
    assert isinstance(projects[0], Project)


@unittest.mock.patch("sonar_client.session.SonarSession.post")
@unittest.mock.patch("sonar_client.client.SonarClient.search_project")
def test_create_sonar_project(mock_search_project, mock_session_post):
    mock_search_project.return_value = []
    mock_session_post.return_value.json.return_value = {
        "project": {
            "organization": "hoshiyosan",
            "key": "hoshiyosan-my-awesome-project",
            "name": "My awesome project",
            "qualifier": "TRK",
            "visibility": "public",
            "lastAnalysisDate": "2022-06-16T14:41:16+0200",
            "revision": "e806983445d408a0cade4e717bc6d14d42879080",
        }
    }

    sonar = SonarClient(SONAR_TOKEN)
    sonar.create_project(
        organization="hoshiyosan",
        project_key="hoshiyosan-my-awesome-project",
        project_title="My awesome project",
    )


@unittest.mock.patch("sonar_client.client.SonarClient.search_project")
def test_create_sonar_project_already_exists(mock_search_project):
    mock_search_project.return_value = [
        Project(
            key="hoshiyosan-my-awesome-project",
            name="My awesome project",
            qualifier="TRK",
        )
    ]

    sonar = SonarClient(SONAR_TOKEN)
    with pytest.raises(SonarProjectAlreadyExistsException):
        sonar.create_project(
            organization="hoshiyosan",
            project_key="hoshiyosan-my-awesome-project",
            project_title="My awesome project",
        )


@unittest.mock.patch("sonar_client.session.SonarSession.post")
def test_main_branch_name(mock_session_post):
    sonar = SonarClient(SONAR_TOKEN)
    sonar.set_main_branch("hoshiyosan-my-awesome-project", "main")


@unittest.mock.patch("sonar_client.session.SonarSession.post")
def test_set_new_code_definition(mock_session_post):
    sonar = SonarClient(SONAR_TOKEN)
    sonar.set_new_code_definition(
        "hoshiyosan-my-awesome-project", NewCodeDefinition.PREVIOUS_VERSION
    )
